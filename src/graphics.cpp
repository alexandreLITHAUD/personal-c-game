/**
 * @file graphics.cpp
 * @author Alexandre Lithaud
 * @brief Graphics class
 * @version 0.1
 * @date 2023-04-13
 * 
 */

#include <SDL2/SDL.h>

#include "../headers/graphics.h"

/**
 * This function creates a window and renderer for graphics using SDL library in C++.
 */
Graphics::Graphics(){
    SDL_CreateWindowAndRenderer(640,480,0,&this->_window,&this->_renderer);
    SDL_SetWindowTitle(this->_window, "Cave Story Copycat");
}

/**
 * This function destroys the SDL window used for graphics.
 */
Graphics::~Graphics(){
    SDL_DestroyWindow(this->_window);
}
