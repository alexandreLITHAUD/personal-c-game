/**
 * @file main.cpp
 * @author Alexandre Lithaud
 * @brief Main Class : Project EntryPoint
 * @version 0.1
 * @date 2023-04-13
 * 
 */

#include <stdio.h>
#include <SDL2/SDL.h>

#include "../headers/game.h"

/**
 * This is a basic C++ main function.
 * 
 * This is the EntryPoint of the Game. 
 * 
 * @param argc The number of command-line arguments passed to the program, including the name of the
 * program itself.
 * @param argv argv is a pointer to an array of characters, where each element of the array is a string
 * representing a command-line argument passed to the program. The first element of the array (argv[0])
 * is the name of the program itself. The second element (argv[1]) is the first command
 * 
 * @return The main function is returning an integer value of 0 if eveything went right, 1 if not.
 */
int main(int argc, char** argv){
    
    Game game;
    return 0;
}

