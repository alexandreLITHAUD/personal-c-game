/**
 * @file game.cpp
 * @author Alexandre Lithaud
 * @brief Game Class
 * @version 0.1
 * @date 2023-04-13
 * 
 */

#include <SDL2/SDL.h>

#include "../headers/game.h"
#include "../headers/graphics.h"
#include "../headers/input.h"

namespace{
    const int FPS = 50;
    const int MAX_FRAME_TIME = 5 * 1000 / FPS;
}


/**
 * This function initializes SDL and starts the game loop.
 */
Game::Game(){
    SDL_Init(SDL_INIT_EVERYTHING);
    this->gameLoop();
}

Game::~Game(){

}

void Game::gameLoop(){
    Graphics graphics;
    Input input;
    SDL_Event event;


    int LAST_UPDATE_TIME = SDL_GetTicks();

    // START GAME LOOP
    while(true){
        input.beginNewFrame();

        if(SDL_PollEvent(&event)){

            if(event.type == SDL_KEYDOWN){
                if(event.key.repeat == 0){
                    input.keyDownEvent(event);
                }
            }

            else if(event.type == SDL_KEYUP){
                input.keyUpEvent(event);
            }

            else if(event.type == SDL_QUIT){
                return;
            }

        }

        if(input.wasKeyPressed(SDL_SCANCODE_ESCAPE) == true){
            return;
        }



        const int CURRENT_TIME_MS = SDL_GetTicks();
        int ELAPSE_TIME_MS = CURRENT_TIME_MS - LAST_UPDATE_TIME;
        this->update(std::min(ELAPSE_TIME_MS,MAX_FRAME_TIME));
        LAST_UPDATE_TIME = CURRENT_TIME_MS;
    }

}

void Game::draw(Graphics &graphics){

}

void Game::update(float elapsedTime){

}