/**
 * @file input.cpp
 * @author Alexandre Lithaud
 * @brief Input Class
 * @version 0.1
 * @date 2023-04-13
 */

#include "../headers/input.h"

/**
 * Will be called every frames
 * The function clears the lists of pressed and released keys for a new input frame.
 */
void Input::beginNewFrame(){
    this->_pressedKeys.clear();
    this->_releasedKeys.clear();
}

/**
 * This function sets a key as released and not held down in the Input class.
 * 
 * @param event The parameter "event" is a constant reference to an SDL_Event object, which represents
 * an event that has occurred in the SDL application. In this case, the function is handling a key up
 * event, which occurs when a keyboard key is released. The SDL_Event object contains information about
 * the specific key that
 */
void Input::keyUpEvent(const SDL_Event& event){
    this->_releasedKeys[event.key.keysym.scancode] = true;
    this->_heldKeys[event.key.keysym.scancode] = false;
}

/**
 * This function sets the corresponding key in the `_pressedKeys` and `_heldKeys` arrays to true when a
 * key is pressed down.
 * 
 * @param event The parameter "event" is a constant reference to an SDL_Event object, which represents
 * an event that has occurred in the SDL application. In this case, the function is handling a key down
 * event, which is triggered when a keyboard key is pressed. The event object contains information
 * about the key that was
 */
void Input::keyDownEvent(const SDL_Event& event){
    this->_pressedKeys[event.key.keysym.scancode] = true;
    this->_heldKeys[event.key.keysym.scancode] = true;
}

/**
 * This function checks if a specific key was pressed.
 * 
 * @param key The parameter "key" is of type SDL_Scancode, which is an enumeration representing the
 * physical keys on a keyboard. It is used to check if a specific key was pressed by the user.
 * 
 * @return A boolean value indicating whether the specified key was pressed or not.
 */
bool Input::wasKeyPressed(SDL_Scancode key){
    return this->_pressedKeys[key];
}

/**
 * This function checks if a specific key was released.
 * 
 * @param key The parameter "key" is of type SDL_Scancode, which is an enumeration representing the
 * physical location of a keyboard key. It is used to check if a specific key has been released.
 * 
 * @return A boolean value indicating whether the specified key was released or not.
 */
bool Input::wasKeyReleased(SDL_Scancode key){
    return this->_releasedKeys[key];
}

/**
 * The function checks if a specific key is being held down.
 * 
 * @param key The parameter "key" is of type SDL_Scancode, which is an enumeration representing the
 * physical keys on a keyboard. It is used to check if a specific key is currently being held down.
 * 
 * @return a boolean value which indicates whether the specified key is currently being held down or
 * not.
 */
bool Input::isKeyHeld(SDL_Scancode key){
    return this->_heldKeys[key];
}