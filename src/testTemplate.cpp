

#include "../headers/testTemplate.h"
#include <stdio.h>

// COLOR
#define RED "\033[0;31m"
#define YELLOW "\033[0;33m"
#define GREEN "\033[0;32m"
#define BLUE "\033[0;34m"
#define NOCOLOR "\033[0m"

bool TestTemplate::check(bool value, char** testname){

    if(value){
        printf(GREEN"TEST PASSED : %s\n"NOCOLOR,testname);
        return false;
    }
    else{
        printf(RED"TEST FAILED : %s\n"NOCOLOR,testname);
        return true;
    }
}