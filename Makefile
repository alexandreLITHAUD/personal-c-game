# Compiler settings
CXX = g++
CXXFLAGS = -std=c++11 -Wall -Wextra -pedantic
SDL_FLAG = `sdl2-config --cflags --libs`

# Directories
SRCDIR = src
INCDIR = headers
OBJDIR = obj

# File lists
SRCS := $(wildcard $(SRCDIR)/*.cpp)
SRC_G := $(filter-out src/test.cpp,$(SRCS))
SRC_T := $(filter-out src/main.cpp,$(SRCS))

OBJ_G := $(SRC_G:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)
OBJ_T := $(SRC_T:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)
DEPS := $(OBJ_T:%.o=%.d)
DEPS += $(OBJ_G:%.o=%.d)

# Targets
TARGET_GAME = caveStoryCopycat
TARGET_TEST = caveStoryCopycatTest

all: $(TARGET_GAME) $(TARGET_TEST)
game: $(TARGET_GAME)
test: $(TARGET_TEST)

$(TARGET_GAME): $(OBJ_G) $(OBJDIR)/main.o
	$(CXX) $(CXXFLAGS) $^ -o $@ $(SDL_FLAG)

$(TARGET_TEST): $(OBJ_T) $(OBJDIR)/test.o
	$(CXX) $(CXXFLAGS) $^ -o $@ $(SDL_FLAG)

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) -MMD -c $< -o $@ $(SDL_FLAG)
	
-include $(DEPS)

.PHONY: clean all game test
clean:
	rm -f $(TARGET_GAME) $(TARGET_TEST) $(OBJ_T) $(OBJ_G) $(DEPS)